using Microsoft.ML.Runtime.Api;

namespace MlTest
{
    public class FoodItemDataPrediction
    {
        [ColumnName("PredictedLabel")]
        public bool IsJunkFood;
    }
}