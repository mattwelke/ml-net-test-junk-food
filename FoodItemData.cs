using Microsoft.ML.Runtime.Api;

namespace MlTest
{
    // All ingredient contents are "grams per 100g".
    public class FoodItemData
    {
        [Column("0")]
        public string Name;

        [Column(ordinal: "1", name: "Label")]
        public bool IsJunkFood;

        [Column("2")]
        public float SaltContent;

        [Column("3")]
        public float SugarContent;

        [Column("4")]
        public float FatContent;
    }
}