using System.Collections.Generic;

namespace MlTest
{
    internal class TestFoodItemData
    {
        /// <summary>
        /// Data the model will test and decide whether or not is considered "junk food".
        /// </summary>
        /// <value></value>
        internal static readonly FoodItemData[] FoodItemData = new FoodItemData[]
        {
            new FoodItemData
            {
                Name = "Peach",
                SaltContent = 0f,
                SugarContent = 8f,
                FatContent = 0.3f
            },
            new FoodItemData
            {
                Name = "Froyo",
                SaltContent = 0.87f,
                SugarContent = 24f,
                FatContent = 6f
            },
            new FoodItemData
            {
                Name = "Cake",
                SaltContent = 0.511f,
                SugarContent = 31f,
                FatContent = 0.3f
            }
        };
    }
}